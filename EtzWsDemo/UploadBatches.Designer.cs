﻿namespace EtzWsDemo
{
    partial class FormUploadBatches
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.federatedLogonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ddlBatchType = new System.Windows.Forms.ComboBox();
            this.gdUploadBatch = new System.Windows.Forms.GroupBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.lblValidFiles = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBatchType = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuBar.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.gdUploadBatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.federatedLogonToolStripMenuItem,
            this.displayBatchesToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1277, 24);
            this.menuBar.TabIndex = 94;
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.logoutToolStripMenuItem.Text = "ETZ Authentication";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // federatedLogonToolStripMenuItem
            // 
            this.federatedLogonToolStripMenuItem.Name = "federatedLogonToolStripMenuItem";
            this.federatedLogonToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.federatedLogonToolStripMenuItem.Text = "Federated Logon";
            this.federatedLogonToolStripMenuItem.Click += new System.EventHandler(this.federatedLogonToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem
            // 
            this.displayBatchesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayDataToolStripMenuItem,
            this.displayBatchesToolStripMenuItem1,
            this.uploadBatchesToolStripMenuItem});
            this.displayBatchesToolStripMenuItem.Name = "displayBatchesToolStripMenuItem";
            this.displayBatchesToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.displayBatchesToolStripMenuItem.Text = "Data";
            // 
            // displayDataToolStripMenuItem
            // 
            this.displayDataToolStripMenuItem.Name = "displayDataToolStripMenuItem";
            this.displayDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.displayDataToolStripMenuItem.Text = "Display Data";
            this.displayDataToolStripMenuItem.Click += new System.EventHandler(this.displayDataToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem1
            // 
            this.displayBatchesToolStripMenuItem1.Name = "displayBatchesToolStripMenuItem1";
            this.displayBatchesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.displayBatchesToolStripMenuItem1.Text = "Display Batches";
            this.displayBatchesToolStripMenuItem1.Click += new System.EventHandler(this.displayBatchesToolStripMenuItem1_Click);
            // 
            // uploadBatchesToolStripMenuItem
            // 
            this.uploadBatchesToolStripMenuItem.Enabled = false;
            this.uploadBatchesToolStripMenuItem.Name = "uploadBatchesToolStripMenuItem";
            this.uploadBatchesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.uploadBatchesToolStripMenuItem.Text = "Upload Batches";
            this.uploadBatchesToolStripMenuItem.Click += new System.EventHandler(this.uploadBatchesToolStripMenuItem_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 588);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1277, 22);
            this.statusBar.TabIndex = 93;
            this.statusBar.Text = "Status: ";
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(162, 17);
            this.statusBarLabel.Text = "Please authenticate with Etz...";
            // 
            // ddlBatchType
            // 
            this.ddlBatchType.FormattingEnabled = true;
            this.ddlBatchType.Location = new System.Drawing.Point(183, 25);
            this.ddlBatchType.Name = "ddlBatchType";
            this.ddlBatchType.Size = new System.Drawing.Size(215, 21);
            this.ddlBatchType.TabIndex = 95;
            // 
            // gdUploadBatch
            // 
            this.gdUploadBatch.Controls.Add(this.btnUpload);
            this.gdUploadBatch.Controls.Add(this.lblValidFiles);
            this.gdUploadBatch.Controls.Add(this.btnBrowse);
            this.gdUploadBatch.Controls.Add(this.txtFile);
            this.gdUploadBatch.Controls.Add(this.label1);
            this.gdUploadBatch.Controls.Add(this.lblBatchType);
            this.gdUploadBatch.Controls.Add(this.ddlBatchType);
            this.gdUploadBatch.Location = new System.Drawing.Point(39, 37);
            this.gdUploadBatch.Name = "gdUploadBatch";
            this.gdUploadBatch.Size = new System.Drawing.Size(485, 148);
            this.gdUploadBatch.TabIndex = 96;
            this.gdUploadBatch.TabStop = false;
            this.gdUploadBatch.Text = "Upload a Batch";
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(19, 112);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(75, 23);
            this.btnUpload.TabIndex = 101;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // lblValidFiles
            // 
            this.lblValidFiles.AutoSize = true;
            this.lblValidFiles.Location = new System.Drawing.Point(180, 86);
            this.lblValidFiles.Name = "lblValidFiles";
            this.lblValidFiles.Size = new System.Drawing.Size(242, 13);
            this.lblValidFiles.TabIndex = 100;
            this.lblValidFiles.Text = "You may upload .json, .xls, .xlst, .csv and .xml files";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(404, 60);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 99;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFile
            // 
            this.txtFile.Enabled = false;
            this.txtFile.Location = new System.Drawing.Point(183, 62);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(215, 20);
            this.txtFile.TabIndex = 98;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "Please select a file to upload:";
            // 
            // lblBatchType
            // 
            this.lblBatchType.AutoSize = true;
            this.lblBatchType.Location = new System.Drawing.Point(16, 28);
            this.lblBatchType.Name = "lblBatchType";
            this.lblBatchType.Size = new System.Drawing.Size(135, 13);
            this.lblBatchType.TabIndex = 96;
            this.lblBatchType.Text = "Please select a batch type:";
            // 
            // formUploadBatches
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 610);
            this.Controls.Add(this.gdUploadBatch);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.statusBar);
            this.Name = "FormUploadBatches";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload Batches";
            this.Load += new System.EventHandler(this.formUploadBatches_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.gdUploadBatch.ResumeLayout(false);
            this.gdUploadBatch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem federatedLogonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadBatchesToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.ComboBox ddlBatchType;
        private System.Windows.Forms.GroupBox gdUploadBatch;
        private System.Windows.Forms.Label lblValidFiles;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBatchType;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.ToolStripMenuItem displayDataToolStripMenuItem;
    }
}