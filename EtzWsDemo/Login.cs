﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EtzWsDemo
{
    public partial class formLogin : Form
    {
        private EtzWsDemoClass etzWsDemoClass; //Local object of EtzWeDemoClass

        public formLogin(EtzWsDemoClass e)
        {
            InitializeComponent();
            etzWsDemoClass = e; //Set the private instance of etzWsDemoClass to the current instance
            menuBar.Enabled = etzWsDemoClass.MyEtzAuthToken != string.Empty; //Enable the menu if the AuthToken is not null
        }

        #region FormMethods
            #region ButtonClicks
                private void btnAuthenticate_Click(object sender, EventArgs e)
                {
                    btnAuthenticate.Enabled = false;
                    AuthenticateUser(txtUsername.Text, txtPassword.Text);
                }
                private void btnLogout_Click(object sender, EventArgs e)
                {
                    Logout(); //Logout of the Web Service
                }
            #endregion
        #endregion

        #region LocalMethods
            //Authenticate the user using EtzAuth
            private bool AuthenticateUser(string username, string password)
            {
                bool returnValue = false;
                try
                {
                    statusBarLabel.Text = "Trying to Authenticate with Etz...";

                    if (username == string.Empty)
                        throw new ApplicationException("Please type your username!");

                    if (password == string.Empty)
                        throw new ApplicationException("Please type your password!");

                    etzWsDemoClass.MyEtzAuthToken = etzWsDemoClass.EtzAuth.EtzAuthToken(username, password); //Try to authenticate the username and password using EtzAuth, returning an AuthToken string
                    txtPassword.Enabled = false;
                    txtUsername.Enabled = false;
                    btnAuthenticate.Enabled = false;
                    btnLogout.Enabled = true;
                    statusBarLabel.Text = "Successfully Authenticated as " + txtUsername.Text;
                    menuBar.Enabled = true;
                    returnValue = true;
                }
                catch (Exception exception)
                {
                    statusBarLabel.Text = "Error: " + exception.Message;
                    returnValue = false;
                    btnAuthenticate.Enabled = true;
                }
                finally
                {
                    txtUsername.Text = string.Empty;
                    txtPassword.Text = string.Empty;
                }
                return returnValue;
            }

            //Logout of the Web Service
            public void Logout()
            {
                try
                {
                    etzWsDemoClass.EtzAuth.EtzAuthTokenDelete(etzWsDemoClass.MyEtzAuthToken);
                    statusBarLabel.Text = "Successfully Logged out of Etz!";
                    txtUsername.Enabled = true;
                    txtPassword.Enabled = true;
                    btnLogout.Enabled = false;
                    btnAuthenticate.Enabled = true;
                }
                catch (Exception exception)
                {
                    statusBarLabel.Text = "Error: " + exception.Message;
                }
            }
        #endregion
        
        #region MenuBar
            private void federatedLogonToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowFederatedLogon(this);
            }
            private void showToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowLogin(this);
            }
            private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.Logout(this);
            }
            private void displayBatchesToolStripMenuItem1_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowDisplayBatches(this);
            }
            private void uploadBatchesToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowUploadBatches(this);
            }
            private void displayDataToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowDisplayData(this);
            }
        #endregion
        
    }
}
