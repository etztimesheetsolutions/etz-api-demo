﻿namespace EtzWsDemo
{
    partial class formDisplayBatches
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.gridResults = new System.Windows.Forms.DataGridView();
            this.ddlBatches = new System.Windows.Forms.ComboBox();
            this.rbPayrollBatches = new System.Windows.Forms.RadioButton();
            this.rbSalesInvoiceBatches = new System.Windows.Forms.RadioButton();
            this.rbPurchaseInvoiceBatches = new System.Windows.Forms.RadioButton();
            this.btnGo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.federatedLogonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbProcessed = new System.Windows.Forms.GroupBox();
            this.rbProcessed = new System.Windows.Forms.RadioButton();
            this.rbUnprocessed = new System.Windows.Forms.RadioButton();
            this.statusBar.SuspendLayout();
            this.gbResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.gbProcessed.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 588);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1277, 22);
            this.statusBar.TabIndex = 0;
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.gridResults);
            this.gbResults.Location = new System.Drawing.Point(12, 177);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(1253, 400);
            this.gbResults.TabIndex = 2;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // gridResults
            // 
            this.gridResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResults.Location = new System.Drawing.Point(16, 19);
            this.gridResults.Name = "gridResults";
            this.gridResults.Size = new System.Drawing.Size(1223, 363);
            this.gridResults.TabIndex = 3;
            // 
            // ddlBatches
            // 
            this.ddlBatches.FormattingEnabled = true;
            this.ddlBatches.Location = new System.Drawing.Point(153, 92);
            this.ddlBatches.Name = "ddlBatches";
            this.ddlBatches.Size = new System.Drawing.Size(161, 21);
            this.ddlBatches.TabIndex = 7;
            // 
            // rbPayrollBatches
            // 
            this.rbPayrollBatches.AutoSize = true;
            this.rbPayrollBatches.Location = new System.Drawing.Point(6, 73);
            this.rbPayrollBatches.Name = "rbPayrollBatches";
            this.rbPayrollBatches.Size = new System.Drawing.Size(56, 17);
            this.rbPayrollBatches.TabIndex = 8;
            this.rbPayrollBatches.TabStop = true;
            this.rbPayrollBatches.Text = "Payroll";
            this.rbPayrollBatches.UseVisualStyleBackColor = true;
            this.rbPayrollBatches.CheckedChanged += new System.EventHandler(this.rbPayrollBatches_CheckedChanged);
            // 
            // rbSalesInvoiceBatches
            // 
            this.rbSalesInvoiceBatches.AutoSize = true;
            this.rbSalesInvoiceBatches.Location = new System.Drawing.Point(6, 96);
            this.rbSalesInvoiceBatches.Name = "rbSalesInvoiceBatches";
            this.rbSalesInvoiceBatches.Size = new System.Drawing.Size(89, 17);
            this.rbSalesInvoiceBatches.TabIndex = 9;
            this.rbSalesInvoiceBatches.TabStop = true;
            this.rbSalesInvoiceBatches.Text = "Sales Invoice";
            this.rbSalesInvoiceBatches.UseVisualStyleBackColor = true;
            this.rbSalesInvoiceBatches.CheckedChanged += new System.EventHandler(this.rbSalesInvoiceBatches_CheckedChanged);
            // 
            // rbPurchaseInvoiceBatches
            // 
            this.rbPurchaseInvoiceBatches.AutoSize = true;
            this.rbPurchaseInvoiceBatches.Location = new System.Drawing.Point(6, 119);
            this.rbPurchaseInvoiceBatches.Name = "rbPurchaseInvoiceBatches";
            this.rbPurchaseInvoiceBatches.Size = new System.Drawing.Size(108, 17);
            this.rbPurchaseInvoiceBatches.TabIndex = 10;
            this.rbPurchaseInvoiceBatches.TabStop = true;
            this.rbPurchaseInvoiceBatches.Text = "Purchase Invoice";
            this.rbPurchaseInvoiceBatches.UseVisualStyleBackColor = true;
            this.rbPurchaseInvoiceBatches.CheckedChanged += new System.EventHandler(this.rbPurchaseInvoiceBatches_CheckedChanged);
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(334, 91);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 11;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gbProcessed);
            this.groupBox1.Controls.Add(this.rbPayrollBatches);
            this.groupBox1.Controls.Add(this.btnGo);
            this.groupBox1.Controls.Add(this.rbSalesInvoiceBatches);
            this.groupBox1.Controls.Add(this.ddlBatches);
            this.groupBox1.Controls.Add(this.rbPurchaseInvoiceBatches);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(422, 144);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batches";
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.federatedLogonToolStripMenuItem,
            this.displayBatchesToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1277, 24);
            this.menuBar.TabIndex = 93;
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.logoutToolStripMenuItem.Text = "ETZ Authentication";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // federatedLogonToolStripMenuItem
            // 
            this.federatedLogonToolStripMenuItem.Name = "federatedLogonToolStripMenuItem";
            this.federatedLogonToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.federatedLogonToolStripMenuItem.Text = "Federated Logon";
            this.federatedLogonToolStripMenuItem.Click += new System.EventHandler(this.federatedLogonToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem
            // 
            this.displayBatchesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayDataToolStripMenuItem,
            this.displayBatchesToolStripMenuItem1,
            this.uploadBatchesToolStripMenuItem});
            this.displayBatchesToolStripMenuItem.Name = "displayBatchesToolStripMenuItem";
            this.displayBatchesToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.displayBatchesToolStripMenuItem.Text = "Data";
            // 
            // displayBatchesToolStripMenuItem1
            // 
            this.displayBatchesToolStripMenuItem1.Enabled = false;
            this.displayBatchesToolStripMenuItem1.Name = "displayBatchesToolStripMenuItem1";
            this.displayBatchesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.displayBatchesToolStripMenuItem1.Text = "Display Batches";
            this.displayBatchesToolStripMenuItem1.Click += new System.EventHandler(this.displayBatchesToolStripMenuItem1_Click);
            // 
            // uploadBatchesToolStripMenuItem
            // 
            this.uploadBatchesToolStripMenuItem.Name = "uploadBatchesToolStripMenuItem";
            this.uploadBatchesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.uploadBatchesToolStripMenuItem.Text = "Upload Batches";
            this.uploadBatchesToolStripMenuItem.Click += new System.EventHandler(this.uploadBatchesToolStripMenuItem_Click);
            // 
            // displayDataToolStripMenuItem
            // 
            this.displayDataToolStripMenuItem.Name = "displayDataToolStripMenuItem";
            this.displayDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.displayDataToolStripMenuItem.Text = "Display Data";
            this.displayDataToolStripMenuItem.Click += new System.EventHandler(this.displayDataToolStripMenuItem_Click);
            // 
            // gbProcessed
            // 
            this.gbProcessed.Controls.Add(this.rbUnprocessed);
            this.gbProcessed.Controls.Add(this.rbProcessed);
            this.gbProcessed.Location = new System.Drawing.Point(9, 19);
            this.gbProcessed.Name = "gbProcessed";
            this.gbProcessed.Size = new System.Drawing.Size(305, 41);
            this.gbProcessed.TabIndex = 12;
            this.gbProcessed.TabStop = false;
            // 
            // rbProcessed
            // 
            this.rbProcessed.AutoSize = true;
            this.rbProcessed.Checked = true;
            this.rbProcessed.Location = new System.Drawing.Point(7, 13);
            this.rbProcessed.Name = "rbProcessed";
            this.rbProcessed.Size = new System.Drawing.Size(75, 17);
            this.rbProcessed.TabIndex = 0;
            this.rbProcessed.TabStop = true;
            this.rbProcessed.Text = "Processed";
            this.rbProcessed.UseVisualStyleBackColor = true;
            this.rbProcessed.CheckedChanged += new System.EventHandler(this.rbProcessed_CheckedChanged);
            // 
            // rbUnprocessed
            // 
            this.rbUnprocessed.AutoSize = true;
            this.rbUnprocessed.Location = new System.Drawing.Point(110, 13);
            this.rbUnprocessed.Name = "rbUnprocessed";
            this.rbUnprocessed.Size = new System.Drawing.Size(89, 17);
            this.rbUnprocessed.TabIndex = 1;
            this.rbUnprocessed.Text = "UnProcessed";
            this.rbUnprocessed.UseVisualStyleBackColor = true;
            this.rbUnprocessed.CheckedChanged += new System.EventHandler(this.rbUnprocessed_CheckedChanged);
            // 
            // formDisplayBatches
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 610);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.statusBar);
            this.Name = "formDisplayBatches";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Display Batch";
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.gbResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.gbProcessed.ResumeLayout(false);
            this.gbProcessed.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.DataGridView gridResults;
        private System.Windows.Forms.ComboBox ddlBatches;
        private System.Windows.Forms.RadioButton rbPayrollBatches;
        private System.Windows.Forms.RadioButton rbSalesInvoiceBatches;
        private System.Windows.Forms.RadioButton rbPurchaseInvoiceBatches;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem federatedLogonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayDataToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbProcessed;
        private System.Windows.Forms.RadioButton rbUnprocessed;
        private System.Windows.Forms.RadioButton rbProcessed;
    }
}