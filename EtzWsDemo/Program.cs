﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EtzWsDemo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            EtzWsDemoClass etzWsDemoClass = new EtzWsDemoClass();
            Application.Run(etzWsDemoClass.FormLogin);
        }
    }
}
