﻿namespace EtzWsDemo
{
    partial class formLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.btnAuthenticate = new System.Windows.Forms.Button();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbAuthenticate = new System.Windows.Forms.GroupBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.federatedLogonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar.SuspendLayout();
            this.gbAuthenticate.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(78, 19);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(173, 20);
            this.txtUsername.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(78, 45);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(173, 20);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(14, 22);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(58, 13);
            this.lblUsername.TabIndex = 2;
            this.lblUsername.Text = "Username:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(14, 48);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password:";
            // 
            // btnAuthenticate
            // 
            this.btnAuthenticate.Location = new System.Drawing.Point(78, 71);
            this.btnAuthenticate.Name = "btnAuthenticate";
            this.btnAuthenticate.Size = new System.Drawing.Size(75, 23);
            this.btnAuthenticate.TabIndex = 4;
            this.btnAuthenticate.Text = "Login";
            this.btnAuthenticate.UseVisualStyleBackColor = true;
            this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 588);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1277, 22);
            this.statusBar.TabIndex = 5;
            this.statusBar.Text = "Status: ";
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(162, 17);
            this.statusBarLabel.Text = "Please authenticate with Etz...";
            // 
            // gbAuthenticate
            // 
            this.gbAuthenticate.Controls.Add(this.btnLogout);
            this.gbAuthenticate.Controls.Add(this.txtUsername);
            this.gbAuthenticate.Controls.Add(this.lblPassword);
            this.gbAuthenticate.Controls.Add(this.txtPassword);
            this.gbAuthenticate.Controls.Add(this.lblUsername);
            this.gbAuthenticate.Controls.Add(this.btnAuthenticate);
            this.gbAuthenticate.Location = new System.Drawing.Point(483, 48);
            this.gbAuthenticate.Name = "gbAuthenticate";
            this.gbAuthenticate.Size = new System.Drawing.Size(284, 109);
            this.gbAuthenticate.TabIndex = 7;
            this.gbAuthenticate.TabStop = false;
            this.gbAuthenticate.Text = "Authentication";
            // 
            // btnLogout
            // 
            this.btnLogout.Enabled = false;
            this.btnLogout.Location = new System.Drawing.Point(159, 71);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 23);
            this.btnLogout.TabIndex = 5;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.federatedLogonToolStripMenuItem,
            this.displayBatchesToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1277, 24);
            this.menuBar.TabIndex = 92;
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.logoutToolStripMenuItem.Enabled = false;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.logoutToolStripMenuItem.Text = "ETZ Authentication";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // federatedLogonToolStripMenuItem
            // 
            this.federatedLogonToolStripMenuItem.Name = "federatedLogonToolStripMenuItem";
            this.federatedLogonToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.federatedLogonToolStripMenuItem.Text = "Federated Logon";
            this.federatedLogonToolStripMenuItem.Click += new System.EventHandler(this.federatedLogonToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem
            // 
            this.displayBatchesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayDataToolStripMenuItem,
            this.displayBatchesToolStripMenuItem1,
            this.uploadBatchesToolStripMenuItem});
            this.displayBatchesToolStripMenuItem.Name = "displayBatchesToolStripMenuItem";
            this.displayBatchesToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.displayBatchesToolStripMenuItem.Text = "Data";
            // 
            // displayBatchesToolStripMenuItem1
            // 
            this.displayBatchesToolStripMenuItem1.Name = "displayBatchesToolStripMenuItem1";
            this.displayBatchesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.displayBatchesToolStripMenuItem1.Text = "Display Batches";
            this.displayBatchesToolStripMenuItem1.Click += new System.EventHandler(this.displayBatchesToolStripMenuItem1_Click);
            // 
            // uploadBatchesToolStripMenuItem
            // 
            this.uploadBatchesToolStripMenuItem.Name = "uploadBatchesToolStripMenuItem";
            this.uploadBatchesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.uploadBatchesToolStripMenuItem.Text = "Upload Batches";
            this.uploadBatchesToolStripMenuItem.Click += new System.EventHandler(this.uploadBatchesToolStripMenuItem_Click);
            // 
            // displayDataToolStripMenuItem
            // 
            this.displayDataToolStripMenuItem.Name = "displayDataToolStripMenuItem";
            this.displayDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.displayDataToolStripMenuItem.Text = "Display Data";
            this.displayDataToolStripMenuItem.Click += new System.EventHandler(this.displayDataToolStripMenuItem_Click);
            // 
            // formLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 610);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.gbAuthenticate);
            this.Controls.Add(this.statusBar);
            this.Name = "formLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.gbAuthenticate.ResumeLayout(false);
            this.gbAuthenticate.PerformLayout();
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button btnAuthenticate;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.GroupBox gbAuthenticate;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem federatedLogonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayDataToolStripMenuItem;
    }
}

