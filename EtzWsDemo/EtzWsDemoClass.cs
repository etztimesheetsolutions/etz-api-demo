﻿
namespace EtzWsDemo
{
    public class EtzWsDemoClass
    {
        public FormFederatedLogon FormFederatedLogon; //Declare a public accessible object for Federated Logon Form
        public formLogin FormLogin; //Declare a public accessible object for EtzAuth Login Form
        public formDisplayBatches FormDisplayBatches; //Declare a public accessible object for Display Batches Form
        public FormUploadBatches FormUploadBatches; //Declare a public accessible object for Upload Batches Form
        public FormDisplayData FormDisplayData; //Declare a public accessible object for Display Data Form

        public EtzAuth EtzAuth = new EtzAuth(); //Declare a public accessible object for EtzAuth
        public EtzFederation EtzFederation = new EtzFederation(); //Declare a public accessible object for EtzFederation
        public EtzInterfaceII EtzInterfaceII = new EtzInterfaceII(); //Declare a public accessible object for EtzInterfaceII
        
        //Declare a public property to store and retrieve the EtzAuthToken
        private string _myEtzAuthToken = string.Empty;
        public string MyEtzAuthToken { get { return _myEtzAuthToken; } set { _myEtzAuthToken = value; } }
            
        public EtzWsDemoClass()
        {
            FormLogin = new formLogin(this); //Create a new instance of the Login Form, passing in this class
            FormFederatedLogon = new FormFederatedLogon(this); //Create a new instance of the Federated Logon Form, passing in this class
            FormDisplayBatches = new formDisplayBatches(this); //Create a new instance of the Display Batches Form, passing in this class
            FormUploadBatches = new FormUploadBatches(this); //Create a new instance of the Upload Batches Form, passing in this class
            FormDisplayData = new FormDisplayData(this); //Create a new instance of the Display Data Form, passing in this class
        }

        public void ShowFederatedLogon(System.Windows.Forms.Form form)
        {
            //If not already the Federated Logon Form
            if (form != FormFederatedLogon)
            {
                form.Hide();
                FormFederatedLogon.Show();
            }
        }

        public void Logout(System.Windows.Forms.Form form)
        {
            form.Hide();
            FormLogin.Logout();
            FormLogin.Show();
        }

        public void ShowLogin(System.Windows.Forms.Form form)
        {
            //If not already the Login Form
            if (form != FormLogin)
            {
                form.Hide();
                FormLogin.Show();
            }
        }

        public void ShowDisplayBatches(System.Windows.Forms.Form form)
        {
            //If not already the Display Batches Form
            if (form != FormDisplayBatches)
            {
                form.Hide();
                FormDisplayBatches.Show();
            }
        }

        public void ShowUploadBatches(System.Windows.Forms.Form form)
        {
            //If not already the Upload Batches Form
            if (form != FormUploadBatches)
            {
                form.Hide();
                FormUploadBatches.Show();
            }
        }

        public void ShowDisplayData(System.Windows.Forms.Form form)
        {
            //If not already the Display Data Form
            if (form != FormDisplayData)
            {
                form.Hide();
                FormDisplayData.Show();
            }
        }
    }
}
