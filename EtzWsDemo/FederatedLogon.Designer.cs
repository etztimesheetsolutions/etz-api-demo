﻿namespace EtzWsDemo
{
    partial class FormFederatedLogon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbUserId = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label67 = new System.Windows.Forms.Label();
            this.btnFedLogin2 = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.txbUserAgencyRef = new System.Windows.Forms.TextBox();
            this.btnFedLogin = new System.Windows.Forms.Button();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.federatedLogonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // txbUserId
            // 
            this.txbUserId.Location = new System.Drawing.Point(158, 45);
            this.txbUserId.Name = "txbUserId";
            this.txbUserId.Size = new System.Drawing.Size(97, 20);
            this.txbUserId.TabIndex = 14;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label67);
            this.groupBox4.Controls.Add(this.txbUserId);
            this.groupBox4.Controls.Add(this.btnFedLogin2);
            this.groupBox4.Controls.Add(this.label66);
            this.groupBox4.Controls.Add(this.txbUserAgencyRef);
            this.groupBox4.Controls.Add(this.btnFedLogin);
            this.groupBox4.Location = new System.Drawing.Point(461, 51);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(352, 87);
            this.groupBox4.TabIndex = 88;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Federated Logon";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(20, 48);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(56, 13);
            this.label67.TabIndex = 15;
            this.label67.Text = "By User Id";
            // 
            // btnFedLogin2
            // 
            this.btnFedLogin2.Location = new System.Drawing.Point(261, 45);
            this.btnFedLogin2.Name = "btnFedLogin2";
            this.btnFedLogin2.Size = new System.Drawing.Size(55, 23);
            this.btnFedLogin2.TabIndex = 13;
            this.btnFedLogin2.Text = "Launch";
            this.btnFedLogin2.UseVisualStyleBackColor = true;
            this.btnFedLogin2.Click += new System.EventHandler(this.btnFedLogin2_Click);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(20, 22);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(103, 13);
            this.label66.TabIndex = 12;
            this.label66.Text = "By User Agency Ref";
            // 
            // txbUserAgencyRef
            // 
            this.txbUserAgencyRef.Location = new System.Drawing.Point(158, 19);
            this.txbUserAgencyRef.Name = "txbUserAgencyRef";
            this.txbUserAgencyRef.Size = new System.Drawing.Size(97, 20);
            this.txbUserAgencyRef.TabIndex = 11;
            // 
            // btnFedLogin
            // 
            this.btnFedLogin.Location = new System.Drawing.Point(261, 19);
            this.btnFedLogin.Name = "btnFedLogin";
            this.btnFedLogin.Size = new System.Drawing.Size(55, 23);
            this.btnFedLogin.TabIndex = 10;
            this.btnFedLogin.Text = "Launch";
            this.btnFedLogin.UseVisualStyleBackColor = true;
            this.btnFedLogin.Click += new System.EventHandler(this.btnFedLogin_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 588);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1277, 22);
            this.statusBar.TabIndex = 90;
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.federatedLogonToolStripMenuItem,
            this.displayBatchesToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(1277, 24);
            this.menuBar.TabIndex = 94;
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.logoutToolStripMenuItem.Text = "ETZ Authentication";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // federatedLogonToolStripMenuItem
            // 
            this.federatedLogonToolStripMenuItem.Enabled = false;
            this.federatedLogonToolStripMenuItem.Name = "federatedLogonToolStripMenuItem";
            this.federatedLogonToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.federatedLogonToolStripMenuItem.Text = "Federated Logon";
            this.federatedLogonToolStripMenuItem.Click += new System.EventHandler(this.federatedLogonToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem
            // 
            this.displayBatchesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayDataToolStripMenuItem,
            this.displayBatchesToolStripMenuItem1,
            this.uploadBatchesToolStripMenuItem});
            this.displayBatchesToolStripMenuItem.Name = "displayBatchesToolStripMenuItem";
            this.displayBatchesToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.displayBatchesToolStripMenuItem.Text = "Data";
            // 
            // displayBatchesToolStripMenuItem1
            // 
            this.displayBatchesToolStripMenuItem1.Name = "displayBatchesToolStripMenuItem1";
            this.displayBatchesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.displayBatchesToolStripMenuItem1.Text = "Display Batches";
            this.displayBatchesToolStripMenuItem1.Click += new System.EventHandler(this.displayBatchesToolStripMenuItem1_Click);
            // 
            // uploadBatchesToolStripMenuItem
            // 
            this.uploadBatchesToolStripMenuItem.Name = "uploadBatchesToolStripMenuItem";
            this.uploadBatchesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.uploadBatchesToolStripMenuItem.Text = "Upload Batches";
            this.uploadBatchesToolStripMenuItem.Click += new System.EventHandler(this.uploadBatchesToolStripMenuItem_Click);
            // 
            // displayDataToolStripMenuItem
            // 
            this.displayDataToolStripMenuItem.Name = "displayDataToolStripMenuItem";
            this.displayDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.displayDataToolStripMenuItem.Text = "Display Data";
            this.displayDataToolStripMenuItem.Click += new System.EventHandler(this.displayDataToolStripMenuItem_Click);
            // 
            // formFederatedLogon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 610);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.groupBox4);
            this.Name = "FormFederatedLogon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Federated Logon";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbUserId;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Button btnFedLogin2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txbUserAgencyRef;
        private System.Windows.Forms.Button btnFedLogin;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem federatedLogonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayDataToolStripMenuItem;
    }
}