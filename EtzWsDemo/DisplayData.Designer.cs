﻿namespace EtzWsDemo
{
    partial class FormDisplayData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.federatedLogonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayBatchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadBatchesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.gridResults = new System.Windows.Forms.DataGridView();
            this.btnSync = new System.Windows.Forms.Button();
            this.txtPlacementList = new System.Windows.Forms.TextBox();
            this.lblPlacementSync = new System.Windows.Forms.Label();
            this.gbDataTypes = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtpEndAssignmentEndDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEndAssignmentNewAgencyRef = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEndAssignmentGo = new System.Windows.Forms.Button();
            this.txtEndAssignmentId = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlMonth = new System.Windows.Forms.ComboBox();
            this.rbTimesheetCostDetail = new System.Windows.Forms.RadioButton();
            this.rbTimesheetCostByAssignmentId = new System.Windows.Forms.RadioButton();
            this.txtAssignmentId = new System.Windows.Forms.TextBox();
            this.chkTimesheetStatus_Rejected = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_AwaitingAuthorisation = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_Cancelled = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_AwaitingApproval = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_TimesheetReceived = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_WaitingOnFax = new System.Windows.Forms.CheckBox();
            this.chkTimesheetStatus_Open = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPrevious = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.btnTimesheetCostGo = new System.Windows.Forms.Button();
            this.gbTimesheets = new System.Windows.Forms.GroupBox();
            this.lblId = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.rbTimesheetCostDetailByAssignment = new System.Windows.Forms.RadioButton();
            this.rbTimesheetDays = new System.Windows.Forms.RadioButton();
            this.rbTimesheetCostDetails = new System.Windows.Forms.RadioButton();
            this.btnTimesheetGo = new System.Windows.Forms.Button();
            this.gbAssignmentRefOptions = new System.Windows.Forms.GroupBox();
            this.ddlCategory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAssignmentRefOptionsGo = new System.Windows.Forms.Button();
            this.gbAssignment = new System.Windows.Forms.GroupBox();
            this.rbAssignmentConsultantDetails = new System.Windows.Forms.RadioButton();
            this.rbAssignmentRateDetails = new System.Windows.Forms.RadioButton();
            this.rbAssignmentDetails = new System.Windows.Forms.RadioButton();
            this.btnAssignmentGo = new System.Windows.Forms.Button();
            this.chkIsLiveOnly = new System.Windows.Forms.CheckBox();
            this.gbSectorBranch = new System.Windows.Forms.GroupBox();
            this.rbSectors = new System.Windows.Forms.RadioButton();
            this.rbBranches = new System.Windows.Forms.RadioButton();
            this.btnSectorBranchGo = new System.Windows.Forms.Button();
            this.menuBar.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.gbResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).BeginInit();
            this.gbDataTypes.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbTimesheets.SuspendLayout();
            this.gbAssignmentRefOptions.SuspendLayout();
            this.gbAssignment.SuspendLayout();
            this.gbSectorBranch.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.federatedLogonToolStripMenuItem,
            this.displayBatchesToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(911, 24);
            this.menuBar.TabIndex = 95;
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.logoutToolStripMenuItem.Text = "ETZ Authentication";
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // federatedLogonToolStripMenuItem
            // 
            this.federatedLogonToolStripMenuItem.Name = "federatedLogonToolStripMenuItem";
            this.federatedLogonToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.federatedLogonToolStripMenuItem.Text = "Federated Logon";
            this.federatedLogonToolStripMenuItem.Click += new System.EventHandler(this.federatedLogonToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem
            // 
            this.displayBatchesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayDataToolStripMenuItem,
            this.displayBatchesToolStripMenuItem1,
            this.uploadBatchesToolStripMenuItem});
            this.displayBatchesToolStripMenuItem.Name = "displayBatchesToolStripMenuItem";
            this.displayBatchesToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.displayBatchesToolStripMenuItem.Text = "Data";
            // 
            // displayDataToolStripMenuItem
            // 
            this.displayDataToolStripMenuItem.Enabled = false;
            this.displayDataToolStripMenuItem.Name = "displayDataToolStripMenuItem";
            this.displayDataToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.displayDataToolStripMenuItem.Text = "Display Data";
            this.displayDataToolStripMenuItem.Click += new System.EventHandler(this.displayDataToolStripMenuItem_Click);
            // 
            // displayBatchesToolStripMenuItem1
            // 
            this.displayBatchesToolStripMenuItem1.Name = "displayBatchesToolStripMenuItem1";
            this.displayBatchesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.displayBatchesToolStripMenuItem1.Text = "Display Batches";
            this.displayBatchesToolStripMenuItem1.Click += new System.EventHandler(this.displayBatchesToolStripMenuItem1_Click);
            // 
            // uploadBatchesToolStripMenuItem
            // 
            this.uploadBatchesToolStripMenuItem.Name = "uploadBatchesToolStripMenuItem";
            this.uploadBatchesToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.uploadBatchesToolStripMenuItem.Text = "Upload Batches";
            this.uploadBatchesToolStripMenuItem.Click += new System.EventHandler(this.uploadBatchesToolStripMenuItem_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 594);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(911, 22);
            this.statusBar.TabIndex = 94;
            // 
            // statusBarLabel
            // 
            this.statusBarLabel.Name = "statusBarLabel";
            this.statusBarLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // gbResults
            // 
            this.gbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbResults.Controls.Add(this.gridResults);
            this.gbResults.Location = new System.Drawing.Point(12, 322);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(888, 258);
            this.gbResults.TabIndex = 96;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // gridResults
            // 
            this.gridResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResults.Location = new System.Drawing.Point(16, 19);
            this.gridResults.Name = "gridResults";
            this.gridResults.Size = new System.Drawing.Size(860, 233);
            this.gridResults.TabIndex = 3;
            // 
            // btnSync
            // 
            this.btnSync.Location = new System.Drawing.Point(6, 67);
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(141, 23);
            this.btnSync.TabIndex = 8;
            this.btnSync.Text = "Sync";
            this.btnSync.UseVisualStyleBackColor = true;
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // txtPlacementList
            // 
            this.txtPlacementList.Location = new System.Drawing.Point(6, 41);
            this.txtPlacementList.Name = "txtPlacementList";
            this.txtPlacementList.Size = new System.Drawing.Size(141, 20);
            this.txtPlacementList.TabIndex = 7;
            // 
            // lblPlacementSync
            // 
            this.lblPlacementSync.AutoSize = true;
            this.lblPlacementSync.Location = new System.Drawing.Point(7, 23);
            this.lblPlacementSync.Name = "lblPlacementSync";
            this.lblPlacementSync.Size = new System.Drawing.Size(106, 13);
            this.lblPlacementSync.TabIndex = 5;
            this.lblPlacementSync.Text = "Placement Sync List:";
            // 
            // gbDataTypes
            // 
            this.gbDataTypes.Controls.Add(this.groupBox3);
            this.gbDataTypes.Controls.Add(this.groupBox2);
            this.gbDataTypes.Controls.Add(this.groupBox1);
            this.gbDataTypes.Controls.Add(this.gbTimesheets);
            this.gbDataTypes.Controls.Add(this.gbAssignmentRefOptions);
            this.gbDataTypes.Controls.Add(this.gbAssignment);
            this.gbDataTypes.Controls.Add(this.gbSectorBranch);
            this.gbDataTypes.Location = new System.Drawing.Point(12, 27);
            this.gbDataTypes.Name = "gbDataTypes";
            this.gbDataTypes.Size = new System.Drawing.Size(888, 289);
            this.gbDataTypes.TabIndex = 97;
            this.gbDataTypes.TabStop = false;
            this.gbDataTypes.Text = "Type";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtpEndAssignmentEndDate);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtEndAssignmentNewAgencyRef);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.btnEndAssignmentGo);
            this.groupBox3.Controls.Add(this.txtEndAssignmentId);
            this.groupBox3.Location = new System.Drawing.Point(551, 144);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(325, 131);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "End Assignment";
            // 
            // dtpEndAssignmentEndDate
            // 
            this.dtpEndAssignmentEndDate.Location = new System.Drawing.Point(105, 47);
            this.dtpEndAssignmentEndDate.Name = "dtpEndAssignmentEndDate";
            this.dtpEndAssignmentEndDate.Size = new System.Drawing.Size(180, 20);
            this.dtpEndAssignmentEndDate.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "New Agency Ref:";
            // 
            // txtEndAssignmentNewAgencyRef
            // 
            this.txtEndAssignmentNewAgencyRef.Location = new System.Drawing.Point(104, 74);
            this.txtEndAssignmentNewAgencyRef.Name = "txtEndAssignmentNewAgencyRef";
            this.txtEndAssignmentNewAgencyRef.Size = new System.Drawing.Size(141, 20);
            this.txtEndAssignmentNewAgencyRef.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "End Date:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Assignment Id:";
            // 
            // btnEndAssignmentGo
            // 
            this.btnEndAssignmentGo.Location = new System.Drawing.Point(104, 102);
            this.btnEndAssignmentGo.Name = "btnEndAssignmentGo";
            this.btnEndAssignmentGo.Size = new System.Drawing.Size(141, 23);
            this.btnEndAssignmentGo.TabIndex = 8;
            this.btnEndAssignmentGo.Text = "Go";
            this.btnEndAssignmentGo.UseVisualStyleBackColor = true;
            this.btnEndAssignmentGo.Click += new System.EventHandler(this.btnEndAssignmentGo_Click);
            // 
            // txtEndAssignmentId
            // 
            this.txtEndAssignmentId.Location = new System.Drawing.Point(104, 20);
            this.txtEndAssignmentId.Name = "txtEndAssignmentId";
            this.txtEndAssignmentId.Size = new System.Drawing.Size(141, 20);
            this.txtEndAssignmentId.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPlacementSync);
            this.groupBox2.Controls.Add(this.btnSync);
            this.groupBox2.Controls.Add(this.txtPlacementList);
            this.groupBox2.Location = new System.Drawing.Point(377, 144);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(168, 131);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bullhorn Placement Sync";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ddlMonth);
            this.groupBox1.Controls.Add(this.rbTimesheetCostDetail);
            this.groupBox1.Controls.Add(this.rbTimesheetCostByAssignmentId);
            this.groupBox1.Controls.Add(this.txtAssignmentId);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_Rejected);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_AwaitingAuthorisation);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_Cancelled);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_AwaitingApproval);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_TimesheetReceived);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_WaitingOnFax);
            this.groupBox1.Controls.Add(this.chkTimesheetStatus_Open);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPrevious);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtYear);
            this.groupBox1.Controls.Add(this.btnTimesheetCostGo);
            this.groupBox1.Location = new System.Drawing.Point(459, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 119);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timesheet Cost Detail";
            // 
            // ddlMonth
            // 
            this.ddlMonth.FormattingEnabled = true;
            this.ddlMonth.Location = new System.Drawing.Point(101, 25);
            this.ddlMonth.Name = "ddlMonth";
            this.ddlMonth.Size = new System.Drawing.Size(134, 21);
            this.ddlMonth.TabIndex = 5;
            // 
            // rbTimesheetCostDetail
            // 
            this.rbTimesheetCostDetail.AutoSize = true;
            this.rbTimesheetCostDetail.Checked = true;
            this.rbTimesheetCostDetail.Location = new System.Drawing.Point(7, 19);
            this.rbTimesheetCostDetail.Name = "rbTimesheetCostDetail";
            this.rbTimesheetCostDetail.Size = new System.Drawing.Size(66, 17);
            this.rbTimesheetCostDetail.TabIndex = 20;
            this.rbTimesheetCostDetail.TabStop = true;
            this.rbTimesheetCostDetail.Text = "By Date:";
            this.rbTimesheetCostDetail.UseVisualStyleBackColor = true;
            this.rbTimesheetCostDetail.CheckedChanged += new System.EventHandler(this.rbTimesheetCostDetail_CheckedChanged);
            // 
            // rbTimesheetCostByAssignmentId
            // 
            this.rbTimesheetCostByAssignmentId.AutoSize = true;
            this.rbTimesheetCostByAssignmentId.Location = new System.Drawing.Point(7, 66);
            this.rbTimesheetCostByAssignmentId.Name = "rbTimesheetCostByAssignmentId";
            this.rbTimesheetCostByAssignmentId.Size = new System.Drawing.Size(106, 17);
            this.rbTimesheetCostByAssignmentId.TabIndex = 19;
            this.rbTimesheetCostByAssignmentId.Text = "By AssignmentId:";
            this.rbTimesheetCostByAssignmentId.UseVisualStyleBackColor = true;
            this.rbTimesheetCostByAssignmentId.CheckedChanged += new System.EventHandler(this.rbTimesheetCostByAssignmentId_CheckedChanged);
            // 
            // txtAssignmentId
            // 
            this.txtAssignmentId.Enabled = false;
            this.txtAssignmentId.Location = new System.Drawing.Point(6, 89);
            this.txtAssignmentId.Name = "txtAssignmentId";
            this.txtAssignmentId.Size = new System.Drawing.Size(132, 20);
            this.txtAssignmentId.TabIndex = 18;
            // 
            // chkTimesheetStatus_Rejected
            // 
            this.chkTimesheetStatus_Rejected.AutoSize = true;
            this.chkTimesheetStatus_Rejected.Checked = true;
            this.chkTimesheetStatus_Rejected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_Rejected.Location = new System.Drawing.Point(275, 77);
            this.chkTimesheetStatus_Rejected.Name = "chkTimesheetStatus_Rejected";
            this.chkTimesheetStatus_Rejected.Size = new System.Drawing.Size(69, 17);
            this.chkTimesheetStatus_Rejected.TabIndex = 17;
            this.chkTimesheetStatus_Rejected.Text = "Rejected";
            this.chkTimesheetStatus_Rejected.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_AwaitingAuthorisation
            // 
            this.chkTimesheetStatus_AwaitingAuthorisation.AutoSize = true;
            this.chkTimesheetStatus_AwaitingAuthorisation.Checked = true;
            this.chkTimesheetStatus_AwaitingAuthorisation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_AwaitingAuthorisation.Location = new System.Drawing.Point(275, 63);
            this.chkTimesheetStatus_AwaitingAuthorisation.Name = "chkTimesheetStatus_AwaitingAuthorisation";
            this.chkTimesheetStatus_AwaitingAuthorisation.Size = new System.Drawing.Size(130, 17);
            this.chkTimesheetStatus_AwaitingAuthorisation.TabIndex = 16;
            this.chkTimesheetStatus_AwaitingAuthorisation.Text = "Awaiting Authorisation";
            this.chkTimesheetStatus_AwaitingAuthorisation.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_Cancelled
            // 
            this.chkTimesheetStatus_Cancelled.AutoSize = true;
            this.chkTimesheetStatus_Cancelled.Checked = true;
            this.chkTimesheetStatus_Cancelled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_Cancelled.Location = new System.Drawing.Point(275, 48);
            this.chkTimesheetStatus_Cancelled.Name = "chkTimesheetStatus_Cancelled";
            this.chkTimesheetStatus_Cancelled.Size = new System.Drawing.Size(73, 17);
            this.chkTimesheetStatus_Cancelled.TabIndex = 15;
            this.chkTimesheetStatus_Cancelled.Text = "Cancelled";
            this.chkTimesheetStatus_Cancelled.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_AwaitingApproval
            // 
            this.chkTimesheetStatus_AwaitingApproval.AutoSize = true;
            this.chkTimesheetStatus_AwaitingApproval.Checked = true;
            this.chkTimesheetStatus_AwaitingApproval.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_AwaitingApproval.Location = new System.Drawing.Point(145, 92);
            this.chkTimesheetStatus_AwaitingApproval.Name = "chkTimesheetStatus_AwaitingApproval";
            this.chkTimesheetStatus_AwaitingApproval.Size = new System.Drawing.Size(111, 17);
            this.chkTimesheetStatus_AwaitingApproval.TabIndex = 14;
            this.chkTimesheetStatus_AwaitingApproval.Text = "Awaiting Approval";
            this.chkTimesheetStatus_AwaitingApproval.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_TimesheetReceived
            // 
            this.chkTimesheetStatus_TimesheetReceived.AutoSize = true;
            this.chkTimesheetStatus_TimesheetReceived.Checked = true;
            this.chkTimesheetStatus_TimesheetReceived.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_TimesheetReceived.Location = new System.Drawing.Point(145, 77);
            this.chkTimesheetStatus_TimesheetReceived.Name = "chkTimesheetStatus_TimesheetReceived";
            this.chkTimesheetStatus_TimesheetReceived.Size = new System.Drawing.Size(124, 17);
            this.chkTimesheetStatus_TimesheetReceived.TabIndex = 13;
            this.chkTimesheetStatus_TimesheetReceived.Text = "Timesheet Received";
            this.chkTimesheetStatus_TimesheetReceived.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_WaitingOnFax
            // 
            this.chkTimesheetStatus_WaitingOnFax.AutoSize = true;
            this.chkTimesheetStatus_WaitingOnFax.Checked = true;
            this.chkTimesheetStatus_WaitingOnFax.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_WaitingOnFax.Location = new System.Drawing.Point(145, 63);
            this.chkTimesheetStatus_WaitingOnFax.Name = "chkTimesheetStatus_WaitingOnFax";
            this.chkTimesheetStatus_WaitingOnFax.Size = new System.Drawing.Size(99, 17);
            this.chkTimesheetStatus_WaitingOnFax.TabIndex = 12;
            this.chkTimesheetStatus_WaitingOnFax.Text = "Waiting On Fax";
            this.chkTimesheetStatus_WaitingOnFax.UseVisualStyleBackColor = true;
            // 
            // chkTimesheetStatus_Open
            // 
            this.chkTimesheetStatus_Open.AutoSize = true;
            this.chkTimesheetStatus_Open.Checked = true;
            this.chkTimesheetStatus_Open.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTimesheetStatus_Open.Location = new System.Drawing.Point(145, 48);
            this.chkTimesheetStatus_Open.Name = "chkTimesheetStatus_Open";
            this.chkTimesheetStatus_Open.Size = new System.Drawing.Size(52, 17);
            this.chkTimesheetStatus_Open.TabIndex = 11;
            this.chkTimesheetStatus_Open.Text = "Open";
            this.chkTimesheetStatus_Open.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Status:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(316, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Previous Months:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(250, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Year:";
            // 
            // txtPrevious
            // 
            this.txtPrevious.Location = new System.Drawing.Point(319, 26);
            this.txtPrevious.Name = "txtPrevious";
            this.txtPrevious.Size = new System.Drawing.Size(79, 20);
            this.txtPrevious.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Month:";
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(253, 26);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(48, 20);
            this.txtYear.TabIndex = 4;
            // 
            // btnTimesheetCostGo
            // 
            this.btnTimesheetCostGo.Location = new System.Drawing.Point(343, 89);
            this.btnTimesheetCostGo.Name = "btnTimesheetCostGo";
            this.btnTimesheetCostGo.Size = new System.Drawing.Size(68, 23);
            this.btnTimesheetCostGo.TabIndex = 2;
            this.btnTimesheetCostGo.Text = "Go";
            this.btnTimesheetCostGo.UseVisualStyleBackColor = true;
            this.btnTimesheetCostGo.Click += new System.EventHandler(this.btnTimesheetCostGo_Click);
            // 
            // gbTimesheets
            // 
            this.gbTimesheets.Controls.Add(this.lblId);
            this.gbTimesheets.Controls.Add(this.txtId);
            this.gbTimesheets.Controls.Add(this.rbTimesheetCostDetailByAssignment);
            this.gbTimesheets.Controls.Add(this.rbTimesheetDays);
            this.gbTimesheets.Controls.Add(this.rbTimesheetCostDetails);
            this.gbTimesheets.Controls.Add(this.btnTimesheetGo);
            this.gbTimesheets.Location = new System.Drawing.Point(121, 19);
            this.gbTimesheets.Name = "gbTimesheets";
            this.gbTimesheets.Size = new System.Drawing.Size(332, 119);
            this.gbTimesheets.TabIndex = 6;
            this.gbTimesheets.TabStop = false;
            this.gbTimesheets.Text = "Timesheets";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(8, 95);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(73, 13);
            this.lblId.TabIndex = 10;
            this.lblId.Text = "AssignmentId:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(82, 92);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(157, 20);
            this.txtId.TabIndex = 9;
            // 
            // rbTimesheetCostDetailByAssignment
            // 
            this.rbTimesheetCostDetailByAssignment.AutoSize = true;
            this.rbTimesheetCostDetailByAssignment.Location = new System.Drawing.Point(6, 66);
            this.rbTimesheetCostDetailByAssignment.Name = "rbTimesheetCostDetailByAssignment";
            this.rbTimesheetCostDetailByAssignment.Size = new System.Drawing.Size(220, 17);
            this.rbTimesheetCostDetailByAssignment.TabIndex = 6;
            this.rbTimesheetCostDetailByAssignment.Text = "Get Timesheet Cost Detail By Assignment";
            this.rbTimesheetCostDetailByAssignment.UseVisualStyleBackColor = true;
            this.rbTimesheetCostDetailByAssignment.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rbTimesheetDays
            // 
            this.rbTimesheetDays.AutoSize = true;
            this.rbTimesheetDays.Location = new System.Drawing.Point(6, 43);
            this.rbTimesheetDays.Name = "rbTimesheetDays";
            this.rbTimesheetDays.Size = new System.Drawing.Size(121, 17);
            this.rbTimesheetDays.TabIndex = 5;
            this.rbTimesheetDays.Text = "Get Timesheet Days";
            this.rbTimesheetDays.UseVisualStyleBackColor = true;
            this.rbTimesheetDays.CheckedChanged += new System.EventHandler(this.rbTimesheetDays_CheckedChanged);
            // 
            // rbTimesheetCostDetails
            // 
            this.rbTimesheetCostDetails.AutoSize = true;
            this.rbTimesheetCostDetails.Checked = true;
            this.rbTimesheetCostDetails.Location = new System.Drawing.Point(6, 20);
            this.rbTimesheetCostDetails.Name = "rbTimesheetCostDetails";
            this.rbTimesheetCostDetails.Size = new System.Drawing.Size(153, 17);
            this.rbTimesheetCostDetails.TabIndex = 4;
            this.rbTimesheetCostDetails.TabStop = true;
            this.rbTimesheetCostDetails.Text = "Get Timesheet Cost Details";
            this.rbTimesheetCostDetails.UseVisualStyleBackColor = true;
            this.rbTimesheetCostDetails.CheckedChanged += new System.EventHandler(this.rbTimesheetCostDetails_CheckedChanged);
            // 
            // btnTimesheetGo
            // 
            this.btnTimesheetGo.Location = new System.Drawing.Point(245, 90);
            this.btnTimesheetGo.Name = "btnTimesheetGo";
            this.btnTimesheetGo.Size = new System.Drawing.Size(81, 23);
            this.btnTimesheetGo.TabIndex = 3;
            this.btnTimesheetGo.Text = "Go";
            this.btnTimesheetGo.UseVisualStyleBackColor = true;
            this.btnTimesheetGo.Click += new System.EventHandler(this.btnTimesheetGo_Click);
            // 
            // gbAssignmentRefOptions
            // 
            this.gbAssignmentRefOptions.Controls.Add(this.ddlCategory);
            this.gbAssignmentRefOptions.Controls.Add(this.label1);
            this.gbAssignmentRefOptions.Controls.Add(this.btnAssignmentRefOptionsGo);
            this.gbAssignmentRefOptions.Location = new System.Drawing.Point(224, 144);
            this.gbAssignmentRefOptions.Name = "gbAssignmentRefOptions";
            this.gbAssignmentRefOptions.Size = new System.Drawing.Size(147, 131);
            this.gbAssignmentRefOptions.TabIndex = 5;
            this.gbAssignmentRefOptions.TabStop = false;
            this.gbAssignmentRefOptions.Text = "Assignment Ref Options";
            // 
            // ddlCategory
            // 
            this.ddlCategory.FormattingEnabled = true;
            this.ddlCategory.Location = new System.Drawing.Point(11, 37);
            this.ddlCategory.Name = "ddlCategory";
            this.ddlCategory.Size = new System.Drawing.Size(127, 21);
            this.ddlCategory.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select the category below";
            // 
            // btnAssignmentRefOptionsGo
            // 
            this.btnAssignmentRefOptionsGo.Location = new System.Drawing.Point(63, 90);
            this.btnAssignmentRefOptionsGo.Name = "btnAssignmentRefOptionsGo";
            this.btnAssignmentRefOptionsGo.Size = new System.Drawing.Size(75, 23);
            this.btnAssignmentRefOptionsGo.TabIndex = 2;
            this.btnAssignmentRefOptionsGo.Text = "Go";
            this.btnAssignmentRefOptionsGo.UseVisualStyleBackColor = true;
            this.btnAssignmentRefOptionsGo.Click += new System.EventHandler(this.btnAssignmentRefOptionsGo_Click);
            // 
            // gbAssignment
            // 
            this.gbAssignment.Controls.Add(this.rbAssignmentConsultantDetails);
            this.gbAssignment.Controls.Add(this.rbAssignmentRateDetails);
            this.gbAssignment.Controls.Add(this.rbAssignmentDetails);
            this.gbAssignment.Controls.Add(this.btnAssignmentGo);
            this.gbAssignment.Controls.Add(this.chkIsLiveOnly);
            this.gbAssignment.Location = new System.Drawing.Point(6, 144);
            this.gbAssignment.Name = "gbAssignment";
            this.gbAssignment.Size = new System.Drawing.Size(212, 131);
            this.gbAssignment.TabIndex = 4;
            this.gbAssignment.TabStop = false;
            this.gbAssignment.Text = "Assignment";
            // 
            // rbAssignmentConsultantDetails
            // 
            this.rbAssignmentConsultantDetails.AutoSize = true;
            this.rbAssignmentConsultantDetails.Location = new System.Drawing.Point(6, 65);
            this.rbAssignmentConsultantDetails.Name = "rbAssignmentConsultantDetails";
            this.rbAssignmentConsultantDetails.Size = new System.Drawing.Size(187, 17);
            this.rbAssignmentConsultantDetails.TabIndex = 6;
            this.rbAssignmentConsultantDetails.Text = "Get Assignment Consultant Details";
            this.rbAssignmentConsultantDetails.UseVisualStyleBackColor = true;
            // 
            // rbAssignmentRateDetails
            // 
            this.rbAssignmentRateDetails.AutoSize = true;
            this.rbAssignmentRateDetails.Location = new System.Drawing.Point(6, 42);
            this.rbAssignmentRateDetails.Name = "rbAssignmentRateDetails";
            this.rbAssignmentRateDetails.Size = new System.Drawing.Size(160, 17);
            this.rbAssignmentRateDetails.TabIndex = 5;
            this.rbAssignmentRateDetails.Text = "Get Assignment Rate Details";
            this.rbAssignmentRateDetails.UseVisualStyleBackColor = true;
            // 
            // rbAssignmentDetails
            // 
            this.rbAssignmentDetails.AutoSize = true;
            this.rbAssignmentDetails.Checked = true;
            this.rbAssignmentDetails.Location = new System.Drawing.Point(6, 19);
            this.rbAssignmentDetails.Name = "rbAssignmentDetails";
            this.rbAssignmentDetails.Size = new System.Drawing.Size(134, 17);
            this.rbAssignmentDetails.TabIndex = 4;
            this.rbAssignmentDetails.TabStop = true;
            this.rbAssignmentDetails.Text = "Get Assignment Details";
            this.rbAssignmentDetails.UseVisualStyleBackColor = true;
            // 
            // btnAssignmentGo
            // 
            this.btnAssignmentGo.Location = new System.Drawing.Point(112, 90);
            this.btnAssignmentGo.Name = "btnAssignmentGo";
            this.btnAssignmentGo.Size = new System.Drawing.Size(81, 23);
            this.btnAssignmentGo.TabIndex = 3;
            this.btnAssignmentGo.Text = "Go";
            this.btnAssignmentGo.UseVisualStyleBackColor = true;
            this.btnAssignmentGo.Click += new System.EventHandler(this.btnAssignmentGo_Click);
            // 
            // chkIsLiveOnly
            // 
            this.chkIsLiveOnly.AutoSize = true;
            this.chkIsLiveOnly.Location = new System.Drawing.Point(6, 93);
            this.chkIsLiveOnly.Name = "chkIsLiveOnly";
            this.chkIsLiveOnly.Size = new System.Drawing.Size(81, 17);
            this.chkIsLiveOnly.TabIndex = 0;
            this.chkIsLiveOnly.Text = "Is Live Only";
            this.chkIsLiveOnly.UseVisualStyleBackColor = true;
            // 
            // gbSectorBranch
            // 
            this.gbSectorBranch.Controls.Add(this.rbSectors);
            this.gbSectorBranch.Controls.Add(this.rbBranches);
            this.gbSectorBranch.Controls.Add(this.btnSectorBranchGo);
            this.gbSectorBranch.Location = new System.Drawing.Point(6, 19);
            this.gbSectorBranch.Name = "gbSectorBranch";
            this.gbSectorBranch.Size = new System.Drawing.Size(109, 119);
            this.gbSectorBranch.TabIndex = 3;
            this.gbSectorBranch.TabStop = false;
            this.gbSectorBranch.Text = "Sector/Branch";
            // 
            // rbSectors
            // 
            this.rbSectors.AutoSize = true;
            this.rbSectors.Checked = true;
            this.rbSectors.Location = new System.Drawing.Point(6, 19);
            this.rbSectors.Name = "rbSectors";
            this.rbSectors.Size = new System.Drawing.Size(81, 17);
            this.rbSectors.TabIndex = 0;
            this.rbSectors.TabStop = true;
            this.rbSectors.Text = "Get Sectors";
            this.rbSectors.UseVisualStyleBackColor = true;
            // 
            // rbBranches
            // 
            this.rbBranches.AutoSize = true;
            this.rbBranches.Location = new System.Drawing.Point(6, 42);
            this.rbBranches.Name = "rbBranches";
            this.rbBranches.Size = new System.Drawing.Size(90, 17);
            this.rbBranches.TabIndex = 1;
            this.rbBranches.Text = "Get Branches";
            this.rbBranches.UseVisualStyleBackColor = true;
            // 
            // btnSectorBranchGo
            // 
            this.btnSectorBranchGo.Location = new System.Drawing.Point(21, 92);
            this.btnSectorBranchGo.Name = "btnSectorBranchGo";
            this.btnSectorBranchGo.Size = new System.Drawing.Size(75, 23);
            this.btnSectorBranchGo.TabIndex = 2;
            this.btnSectorBranchGo.Text = "Go";
            this.btnSectorBranchGo.UseVisualStyleBackColor = true;
            this.btnSectorBranchGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // FormDisplayData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 616);
            this.Controls.Add(this.gbDataTypes);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.statusBar);
            this.MinimumSize = new System.Drawing.Size(927, 655);
            this.Name = "FormDisplayData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Display Data";
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.gbResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).EndInit();
            this.gbDataTypes.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbTimesheets.ResumeLayout(false);
            this.gbTimesheets.PerformLayout();
            this.gbAssignmentRefOptions.ResumeLayout(false);
            this.gbAssignmentRefOptions.PerformLayout();
            this.gbAssignment.ResumeLayout(false);
            this.gbAssignment.PerformLayout();
            this.gbSectorBranch.ResumeLayout(false);
            this.gbSectorBranch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem federatedLogonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayBatchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uploadBatchesToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLabel;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.DataGridView gridResults;
        private System.Windows.Forms.GroupBox gbDataTypes;
        private System.Windows.Forms.Button btnSectorBranchGo;
        private System.Windows.Forms.RadioButton rbBranches;
        private System.Windows.Forms.RadioButton rbSectors;
        private System.Windows.Forms.GroupBox gbAssignment;
        private System.Windows.Forms.CheckBox chkIsLiveOnly;
        private System.Windows.Forms.GroupBox gbSectorBranch;
        private System.Windows.Forms.Button btnAssignmentGo;
        private System.Windows.Forms.RadioButton rbAssignmentConsultantDetails;
        private System.Windows.Forms.RadioButton rbAssignmentRateDetails;
        private System.Windows.Forms.RadioButton rbAssignmentDetails;
        private System.Windows.Forms.GroupBox gbAssignmentRefOptions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlMonthddlCategory;
        private System.Windows.Forms.Button btnAssignmentRefOptionsGo;
        private System.Windows.Forms.GroupBox gbTimesheets;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.RadioButton rbTimesheetCostDetailByAssignment;
        private System.Windows.Forms.RadioButton rbTimesheetDays;
        private System.Windows.Forms.RadioButton rbTimesheetCostDetails;
        private System.Windows.Forms.Button btnTimesheetGo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTimesheetCostGo;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_Cancelled;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_AwaitingApproval;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_TimesheetReceived;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_WaitingOnFax;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_Open;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPrevious;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_Rejected;
        private System.Windows.Forms.CheckBox chkTimesheetStatus_AwaitingAuthorisation;
        private System.Windows.Forms.TextBox txtAssignmentId;
        private System.Windows.Forms.RadioButton rbTimesheetCostDetail;
        private System.Windows.Forms.RadioButton rbTimesheetCostByAssignmentId;
        private System.Windows.Forms.ComboBox ddlMonth;
        private System.Windows.Forms.ComboBox ddlCategory;
        private System.Windows.Forms.Label lblPlacementSync;
        private System.Windows.Forms.TextBox txtPlacementList;
        private System.Windows.Forms.Button btnSync;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtpEndAssignmentEndDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEndAssignmentNewAgencyRef;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnEndAssignmentGo;
        private System.Windows.Forms.TextBox txtEndAssignmentId;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}