﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace EtzWsDemo
{
    public partial class FormFederatedLogon : Form
    {
        private readonly EtzWsDemoClass _etzWsDemoClass; //Local object of EtzWeDemoClass

        public FormFederatedLogon(EtzWsDemoClass e)
        {
            InitializeComponent();
            _etzWsDemoClass = e; //Set the private instance of etzWsDemoClass to the current instance
        }

        private void btnFedLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the EtzFederation Token from EtzFederation for the specified UserAgencyRef
                string rawEtzFederationToken = _etzWsDemoClass.EtzFederation.EtzFederationTokenForTargetUserAgencyReference(_etzWsDemoClass.MyEtzAuthToken, txbUserAgencyRef.Text);
                LoadFederatedPage(rawEtzFederationToken); //Load the Federated Page
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error:" + exception.Message;
                return;
            }
        }

        private void btnFedLogin2_Click(object sender, EventArgs e)
        {
            try
            {
                //Get the EtzFederation Token from EtzFederation for the specified UserId
                string rawEtzFederationToken = _etzWsDemoClass.EtzFederation.EtzFederationTokenForTargetEtzUserId(_etzWsDemoClass.MyEtzAuthToken, Convert.ToInt32(txbUserId.Text));
                LoadFederatedPage(rawEtzFederationToken); //Load the Federated Page
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error:" + exception.Message;
                return;
            }
        }

        private void LoadFederatedPage(string rawEtzFederationToken)
        {
            //Get the Federated Authentication URL from the EtzFederation object using the EtzAuthToken and append the UrlEncoded string of rawEtzFederationToken
            string urlReturned = _etzWsDemoClass.EtzFederation.EtzFederatedAuthenticationURL(_etzWsDemoClass.MyEtzAuthToken) + System.Web.HttpUtility.UrlEncode(rawEtzFederationToken);

            //Start an instance of Internet Explorer with the returned Url
            Process.Start("IExplore.exe", urlReturned);
            statusBarLabel.Text = "Success";
        }
     
        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowLogin(this);
        }
        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.Logout(this);
        }
        private void federatedLogonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowFederatedLogon(this);
        }
        private void displayBatchesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowDisplayBatches(this);
        }
        private void uploadBatchesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowUploadBatches(this);
        }
        private void displayDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowDisplayData(this);
        }
    }
}
