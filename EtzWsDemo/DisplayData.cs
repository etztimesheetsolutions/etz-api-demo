﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EtzWsDemo
{
    public partial class FormDisplayData : Form
    {
        private readonly EtzWsDemoClass _etzWsDemoClass; //Local object of EtzWeDemoClass

        public FormDisplayData(EtzWsDemoClass e)
        {
            InitializeComponent();
            _etzWsDemoClass = e; //Set the private instance of etzWsDemoClass to the current instance
            PopulateCategories(); //Populate the Category Dropdown List
            PopulateMonths(); //Populate the Month Dropdown List
            txtYear.Text = DateTime.Now.Year.ToString(); //Preset the Year to the current Year
        }

        #region FormMethods
        #region ButtonClicks

        private void btnGo_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            try
            {
                if (rbBranches.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetBranchList(_etzWsDemoClass.MyEtzAuthToken); //Get the Branch List

                if (rbSectors.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetSectorList(_etzWsDemoClass.MyEtzAuthToken); //Get the Sector List

                PopulateResults(dt); //Populate the Results from the DataTable
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error detected: " + exception.Message;
                return;
            }
        }

        private void btnAssignmentGo_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            try
            {
                if (rbAssignmentDetails.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetAssignmentDetailList(_etzWsDemoClass.MyEtzAuthToken, chkIsLiveOnly.Checked); //Get the Assignment Detail List

                if (rbAssignmentRateDetails.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetAssignmentRateDetailList(_etzWsDemoClass.MyEtzAuthToken, chkIsLiveOnly.Checked); //Get the Assignment Rate Detail List

                if (rbAssignmentConsultantDetails.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetAssignmentConsultantDetailList(_etzWsDemoClass.MyEtzAuthToken, chkIsLiveOnly.Checked); //Get the Assignment Consultant Detail List

                PopulateResults(dt); //Populate the Results from the DataTable
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error detected: " + exception.Message;
                return;
            }
        }

        private void btnTimesheetGo_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            try
            {
                if (txtId.Text == string.Empty)
                    throw new ApplicationException("Please enter an " + (rbTimesheetCostDetailByAssignment.Checked ? "Assignment Id!" : "Timesheet Id!"));

                int id = Convert.ToInt32(txtId.Text);

                if (rbTimesheetCostDetails.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetTimesheetCostDetail(_etzWsDemoClass.MyEtzAuthToken, id); //Get the Timesheet Cost Detail by user inputted TimesheetId

                if (rbTimesheetDays.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetTimesheetDay(_etzWsDemoClass.MyEtzAuthToken, id); //Get the Timesheet Days by user inputted TimesheetId

                if (rbTimesheetCostDetailByAssignment.Checked)
                    dt = _etzWsDemoClass.EtzInterfaceII.GetTimesheetCostDetailListByAssignmentId(_etzWsDemoClass.MyEtzAuthToken, id); //Get the Timesheet Cost Details by user inputted AssignmentId

                PopulateResults(dt); //Populate the Results from the DataTable
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error detected: " + exception.Message;
                return;
            }
        }

        private void btnTimesheetCostGo_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            try
            {
                string statusCsv = GetStatusCsv();
                if (rbTimesheetCostByAssignmentId.Checked)
                {
                    if (txtAssignmentId.Text == string.Empty)
                        throw new ApplicationException("Please enter an Assignment Id!");

                    int id = Convert.ToInt32(txtAssignmentId.Text);

                    dt = _etzWsDemoClass.EtzInterfaceII.GetTimesheetCostDetailListByAssignmentIdAndStatusCsv(_etzWsDemoClass.MyEtzAuthToken, id, statusCsv); //Get the Timesheet Cost Detail List By AssignmentId and Selected StatusCsv
                }

                if (rbTimesheetCostDetail.Checked)
                {
                    if (txtYear.Text == string.Empty)
                        throw new ApplicationException("Please enter the start year!");

                    if (txtPrevious.Text == string.Empty)
                        throw new ApplicationException("Please enter the number of previous months!");

                    int month = ddlMonth.SelectedIndex + 1;
                    int year = Convert.ToInt32(txtYear.Text);
                    int previous = Convert.ToInt32(txtPrevious.Text);

                    dt = _etzWsDemoClass.EtzInterfaceII.GetTimesheetCostDetailList(_etzWsDemoClass.MyEtzAuthToken, statusCsv, month, year, previous); //Get the Timesheet Cost Detail List by Selected StatusCsv, Starting Month, Starting Year, Months to back date for
                }

                PopulateResults(dt); //Populate the Results from the DataTable
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error detected: " + exception.Message;
                return;
            }
        }

        private void btnAssignmentRefOptionsGo_Click(object sender, EventArgs e)
        {
            int refOption = ddlCategory.SelectedIndex + 1;
            DataTable dt = _etzWsDemoClass.EtzInterfaceII.GetAssignmentRefOptionQuestionList(_etzWsDemoClass.MyEtzAuthToken, refOption); //Get Assignment RefOption Question List by RefOption
            PopulateResults(dt); //Populate the Results from the DataTable
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            var listInt = new List<int>();
            var split = txtPlacementList.Text.Split(',').ToList();
            split.ForEach(s => listInt.Add(Convert.ToInt32(s)));
            if (listInt.Count > 1)
                _etzWsDemoClass.EtzInterfaceII.BullhornSyncPlacmentRequestList(_etzWsDemoClass.MyEtzAuthToken, listInt.ToArray());
            else
                _etzWsDemoClass.EtzInterfaceII.BullhornSyncPlacmentRequest(_etzWsDemoClass.MyEtzAuthToken, Convert.ToInt32(txtPlacementList.Text));
        }

        private void btnEndAssignmentGo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEndAssignmentId.Text == string.Empty)
                    throw new ApplicationException("Please enter an Assignment Id!");

                _etzWsDemoClass.EtzInterfaceII.EndAssignment(_etzWsDemoClass.MyEtzAuthToken, Convert.ToInt32(txtEndAssignmentId.Text), dtpEndAssignmentEndDate.Value, txtEndAssignmentNewAgencyRef.Text.Trim());
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error detected: " + exception.Message;
                return;
            }
        }

        #endregion

        #region RadioButtonChanges
        private void rbTimesheetCostDetails_CheckedChanged(object sender, EventArgs e)
        {
            lblId.Text = "TimesheetId:";
        }

        private void rbTimesheetDays_CheckedChanged(object sender, EventArgs e)
        {
            lblId.Text = "TimesheetId:";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            lblId.Text = "AssignmentId:";
        }

        private void rbTimesheetCostDetail_CheckedChanged(object sender, EventArgs e)
        {
            ddlMonth.Enabled = rbTimesheetCostDetail.Checked;
            txtYear.Enabled = rbTimesheetCostDetail.Checked;
            txtPrevious.Enabled = rbTimesheetCostDetail.Checked;
        }

        private void rbTimesheetCostByAssignmentId_CheckedChanged(object sender, EventArgs e)
        {
            txtAssignmentId.Enabled = rbTimesheetCostByAssignmentId.Checked;
        }

        #endregion
        #endregion

        #region LocalMethods
            //Populate the Month Dropdown List
            private void PopulateMonths()
            {
                ddlMonth.Items.Clear();
                ddlMonth.Items.Add("January");
                ddlMonth.Items.Add("February");
                ddlMonth.Items.Add("March");
                ddlMonth.Items.Add("April");
                ddlMonth.Items.Add("May");
                ddlMonth.Items.Add("June");
                ddlMonth.Items.Add("July");
                ddlMonth.Items.Add("August");
                ddlMonth.Items.Add("September");
                ddlMonth.Items.Add("October");
                ddlMonth.Items.Add("November");
                ddlMonth.Items.Add("December");
                ddlMonth.SelectedIndex = DateTime.Now.Month - 1;
            }

            //Populate the Category Dropdown List
            private void PopulateCategories()
            {
                ddlCategory.Items.Clear();
                ddlCategory.Items.Add("Catagory 1");
                ddlCategory.Items.Add("Category 2");
                ddlCategory.Items.Add("Category 3");
                ddlCategory.Items.Add("Category 4");
                ddlCategory.Items.Add("Category 5");
                ddlCategory.Items.Add("Category 6");
                ddlCategory.SelectedIndex = 0;
            }

            //Populate the Results Grid from a DataTable
            private void PopulateResults(DataTable dt)
            {
                if (dt.Columns.Contains("RowVersion"))
                    dt.Columns.Remove("RowVersion");

                gridResults.DataSource = dt;
                gbResults.Text = "Results (" + dt.Rows.Count + ")";
            }

            //Create a string of StatusCsv based on the Status Checkbox's
            private string GetStatusCsv()
            {
                string statusCsv = string.Empty;

                if (chkTimesheetStatus_Open.Checked)
                    statusCsv += "0,";
                if (chkTimesheetStatus_WaitingOnFax.Checked)
                    statusCsv += "1,";
                if (chkTimesheetStatus_TimesheetReceived.Checked)
                    statusCsv += "2,";
                if (chkTimesheetStatus_AwaitingApproval.Checked)
                    statusCsv += "3,";
                if (chkTimesheetStatus_Cancelled.Checked)
                    statusCsv += "4,";
                if (chkTimesheetStatus_AwaitingAuthorisation.Checked)
                    statusCsv += "5,";
                if (chkTimesheetStatus_Rejected.Checked)
                    statusCsv += "6,";

                if (statusCsv.Length > 0)
                    statusCsv = statusCsv.Remove(statusCsv.LastIndexOf(","));

                return statusCsv;
            }
        #endregion

        #region MenuBar
        //Methods for switching between instantiated Forms
        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowLogin(this);
        }
        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.Logout(this);
        }
        private void federatedLogonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowFederatedLogon(this);
        }
        private void displayDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowDisplayData(this);
        }

        private void displayBatchesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowDisplayBatches(this);
        }

        private void uploadBatchesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowUploadBatches(this);
        }

        #endregion
    }
}
