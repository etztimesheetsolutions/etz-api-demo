﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace EtzWsDemo
{
    public partial class FormUploadBatches : Form
    {
        private readonly EtzWsDemoClass _etzWsDemoClass;

        public FormUploadBatches(EtzWsDemoClass e)
        {
            InitializeComponent();
            _etzWsDemoClass = e; //Set the private instance of etzWsDemoClass to the current instance
            openFileDialog.FileOk += openFileDialog_FileOk; //Create an event handler to handle FileOk on the openFileDialog
            ResetForm(); //Reset the form to blank
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog.Multiselect = false; //Disable selecting Multiple Files
            //Add the filters for the File Types Etz can accept (*.json, *.xls, *.xlsx, *.xml, *.csv)
            openFileDialog.Filter = "Comma Seperated Values (*.csv)|*.csv|Excel 2003 (*.xls)|*.xls|Excel 2007/2010 (*.xlsx)|*.xlsx|XML File (*.xml)|*.xml|JSON (*.json)|*.json";
            openFileDialog.Title = "Select a file to upload to Etz..."; //Set the openFileDialog Title Bar Text
            openFileDialog.ShowDialog(); //Show the openFileDialog
        }
        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFile.Text == string.Empty)
                    throw new ApplicationException("Please select a file to upload!");
                if (!File.Exists(openFileDialog.FileName))
                    throw new ApplicationException("The file you are trying to upload does not exist!");

                FileStream fileStream = File.OpenRead(openFileDialog.FileName); //Create a FileStream and Open and Read the selected File from openFileDialog
                byte[] file = new byte[fileStream.Length]; //Create a new byte Array based on the length of the FileStream
                fileStream.Read(file, 0, file.Length); //Read the FileStream into the byte Array 'file'

                switch (ddlBatchType.SelectedIndex)
                {
                    case 0: //Assignments
                        _etzWsDemoClass.EtzInterfaceII.UploadAssignmentList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Assignments from 'file' with the current Filename
                        break;
                    case 1: //Assignment Rates
                        _etzWsDemoClass.EtzInterfaceII.UploadAssignmentRateList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Assignment Rates from 'file' with the current Filename
                        break;
                    case 2: //Clients
                        _etzWsDemoClass.EtzInterfaceII.UploadClientList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Clients from 'file' with the current Filename
                        break;
                    case 3: //Client Authorisers
                        _etzWsDemoClass.EtzInterfaceII.UploadClientAuthoriserList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Client Authorisers from 'file' with the current Filename
                        break;
                    case 4: //Consultants
                        _etzWsDemoClass.EtzInterfaceII.UploadConsultantList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Consultants from 'file' with the current Filename
                        break;
                    case 5: //Consultant Commission Splits
                        _etzWsDemoClass.EtzInterfaceII.UploadConsultantCommissionSplitList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Consultant Commission Splits from 'file' with the current Filename
                        break;
                    case 6: //LtdCompany's
                        _etzWsDemoClass.EtzInterfaceII.UploadLtdCompanyList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of LtdCompany's from 'file' with the current Filename
                        break;
                    case 7: //Permanent Assignments
                        _etzWsDemoClass.EtzInterfaceII.UploadPermanentAssignmentList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Permanent Assignments from 'file' with the current Filename
                        break;
                    case 8: //Timesheets
                        _etzWsDemoClass.EtzInterfaceII.UploadTimesheetList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Timesheets from 'file' with the current Filename
                        break;
                    case 9: //Candidates
                        _etzWsDemoClass.EtzInterfaceII.UploadCandidateList(_etzWsDemoClass.MyEtzAuthToken, txtFile.Text, file); //Upload List of Candidates from 'file' with the current Filename
                        break;
                }
                ResetForm(); //Clear the Form
                statusBarLabel.Text = "File Successfully uploaded!";
            }
            catch (Exception exception)
            {
                statusBarLabel.Text = "Error: " + exception.Message;
            }
        }
      
        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            //Display only the Filename and Extension in the TextBox
            txtFile.Text = openFileDialog.FileName.Remove(0, openFileDialog.FileName.LastIndexOf(@"\") + 1);
        }
      
        //Available Batch Types
        private void PopulateBatchTypes()
        {
            ddlBatchType.Items.Clear();
            ddlBatchType.Items.Add("Assignments");
            ddlBatchType.Items.Add("Assignment Rates");
            ddlBatchType.Items.Add("Clients");
            ddlBatchType.Items.Add("Client Authorisers");
            ddlBatchType.Items.Add("Consultants");
            ddlBatchType.Items.Add("Consultant Commission Splits");
            ddlBatchType.Items.Add("LtdCompany's");
            ddlBatchType.Items.Add("Permanent Assignments");
            ddlBatchType.Items.Add("Timesheets");
            ddlBatchType.Items.Add("Candidates");
        }
        private void ResetForm()
        {
            PopulateBatchTypes(); //Populate the Available Batch Types
            txtFile.Text = string.Empty;
        }
      
        private void federatedLogonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowFederatedLogon(this);
        }
        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowLogin(this);
        }
        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.Logout(this);
        }
        private void displayBatchesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowDisplayBatches(this);
        }
        private void uploadBatchesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _etzWsDemoClass.ShowUploadBatches(this);
        }
        private void displayDataToolStripMenuItem_Click(object sender, EventArgs e)
            {
                _etzWsDemoClass.ShowDisplayData(this);
            }
      
        private void formUploadBatches_Load(object sender, EventArgs e)
            {

            }
    }
}
