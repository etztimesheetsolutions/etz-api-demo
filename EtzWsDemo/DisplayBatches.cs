﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EtzWsDemo
{
    public partial class formDisplayBatches : Form
    {
        private EtzWsDemoClass etzWsDemoClass; //Local object of EtzWeDemoClass
        private SelectedBacthTypeEnum selectedBatchTypeEnum = SelectedBacthTypeEnum.Unknown; //Private Variable to stored SelectedBatchTypeEnum
        private enum SelectedBacthTypeEnum //Enum of different Batch Types
        {
            Unknown,
            SalesInvoice,
            PurchaseInvoice,
            Payroll
        }

        public formDisplayBatches(EtzWsDemoClass e)
        {
            InitializeComponent();
            etzWsDemoClass = e; //Set the private instance of etzWsDemoClass to the current instance
        }

        #region FormMethods
            #region ButtonClicks
                private void btnGo_Click(object sender, EventArgs e)
                {
                    try
                    {
                        int batchId = 0;
                        int.TryParse(ddlBatches.SelectedValue.ToString(), out batchId);
                        DataTable dt = null;

                        if (batchId > 0)
                        {
                            //Depending on the Selected Batch Type
                            switch (selectedBatchTypeEnum)
                            {
                                case SelectedBacthTypeEnum.Payroll:
                                    dt = etzWsDemoClass.EtzInterfaceII.GetPayrollTimesheetList(etzWsDemoClass.MyEtzAuthToken, batchId); //Get the Payroll Timesheet List for Selected BatchId
                                    break;
                                case SelectedBacthTypeEnum.SalesInvoice:
                                    dt = etzWsDemoClass.EtzInterfaceII.GetSalesInvoiceList(etzWsDemoClass.MyEtzAuthToken, batchId); //Get the Sales Invoice List for Selected BatchId
                                    break;
                                case SelectedBacthTypeEnum.PurchaseInvoice:
                                    dt = etzWsDemoClass.EtzInterfaceII.GetPurchaseInvoiceList(etzWsDemoClass.MyEtzAuthToken, batchId); //Get the Purchase Invoice List for Selected BatchId
                                    break;
                            }
                        }

                        if (dt != null)
                            DisplayResults(dt);
                    }
                    catch (Exception exception)
                    {
                        statusBarLabel.Text = "Error: " + exception.Message;
                    }
                }
            #endregion

            #region RadioButtonChanges
                private void rbPayrollBatches_CheckedChanged(object sender, EventArgs e)
                {
                    //Populate the BatchList based on the Payroll Type
                    PopulateBatches(SelectedBacthTypeEnum.Payroll);
                }
                private void rbSalesInvoiceBatches_CheckedChanged(object sender, EventArgs e)
                {
                    //Populate the BatchList based on the Sales Invoice Type
                    PopulateBatches(SelectedBacthTypeEnum.SalesInvoice);
                }
                private void rbPurchaseInvoiceBatches_CheckedChanged(object sender, EventArgs e)
                {
                    //Populate the BatchList based on the Purchase Invoice Type
                    PopulateBatches(SelectedBacthTypeEnum.PurchaseInvoice);
                }
            #endregion
        #endregion

        #region LocalMethods
            //Populate the List Of Batches based on the Selection
            private void PopulateBatchList(DataTable dt, SelectedBacthTypeEnum selectedType)
            {
                if (selectedType != SelectedBacthTypeEnum.Unknown)
                {
                    gridResults.DataSource = new DataTable();
                    ddlBatches.DataSource = dt;
                    ddlBatches.ValueMember = "BatchId";
                    ddlBatches.DisplayMember = "DateTimeProcessed";
                    selectedBatchTypeEnum = selectedType;
                }
            }

            //Display the Results from the DataTable
            private void DisplayResults(DataTable dt)
            {
                gridResults.DataSource = dt;
                statusBarLabel.Text = "Success!";
                gbResults.Text = "Displaying " + selectedBatchTypeEnum + " (" + dt.Rows.Count + ")";
            }

            //Populate the BatchList based on the selected Type
            //Use the Public instantiated object EtzInterfaceII from etzWsDemoClass
            private void PopulateBatches(SelectedBacthTypeEnum batchTypeEnum)
            {
                DataTable dt = null;
                try
                {
                    switch (batchTypeEnum)
                    {
                        case SelectedBacthTypeEnum.PurchaseInvoice:
                            if (rbUnprocessed.Checked)
                                dt = etzWsDemoClass.EtzInterfaceII.GetUnprocessedPurchaseInvoiceBatchList(etzWsDemoClass.MyEtzAuthToken); //Get the Unprocessed Purchase Invoice Batch List from EtzInterfaceII
                            else
                                dt = etzWsDemoClass.EtzInterfaceII.GetPurchaseInvoiceBatchList(etzWsDemoClass.MyEtzAuthToken); //Get the Purchase Invoice Batch List from EtzInterfaceII
                            break;
                        case SelectedBacthTypeEnum.SalesInvoice:
                            if (rbUnprocessed.Checked)
                            dt = etzWsDemoClass.EtzInterfaceII.GetUnprocessedSalesInvoiceBatchList(etzWsDemoClass.MyEtzAuthToken); //Get the Unprocessed Sales Invoice Batch List from EtzInterfaceII
                        else
                            dt = etzWsDemoClass.EtzInterfaceII.GetSalesInvoiceBatchList(etzWsDemoClass.MyEtzAuthToken); //Get the Sales Invoice Batch List from EtzInterfaceII
                            break;
                        case SelectedBacthTypeEnum.Payroll:
                            if (rbUnprocessed.Checked)
                            dt = etzWsDemoClass.EtzInterfaceII.GetUnprocessedPayrollBatchList(etzWsDemoClass.MyEtzAuthToken); //Get the Unprocessed Payroll Batch List from EtzInterfaceII
                        else
                            dt = etzWsDemoClass.EtzInterfaceII.GetPayrollBatchList(etzWsDemoClass.MyEtzAuthToken); //Get thePayroll Batch List from EtzInterfaceII
                            break;
                    }
                    if (batchTypeEnum != SelectedBacthTypeEnum.Unknown)
                        PopulateBatchList(dt, batchTypeEnum);
                }
                catch (Exception exception)
                {
                    statusBarLabel.Text = "Error detected: " + exception.Message;
                    return;
                }
            }
        #endregion

        #region MenuBar
            //Menu Bar Methods to switch between Forms
            private void showToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowLogin(this);
            }
            private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.Logout(this);
            }
            private void federatedLogonToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowFederatedLogon(this);
            }
            private void displayBatchesToolStripMenuItem1_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowDisplayBatches(this);
            }
            private void uploadBatchesToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowUploadBatches(this);
            }
            private void displayDataToolStripMenuItem_Click(object sender, EventArgs e)
            {
                etzWsDemoClass.ShowDisplayData(this);
            }
            private void rbProcessed_CheckedChanged(object sender, EventArgs e)
            {
                PopulateBatches(selectedBatchTypeEnum);
            }
            private void rbUnprocessed_CheckedChanged(object sender, EventArgs e)
            {
                PopulateBatches(selectedBatchTypeEnum);
            }
        #endregion
    }
}
